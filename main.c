#include <ncurses.h>

int main() {
  initscr();
  raw();
  keypad(stdscr, TRUE);
  noecho();

  printw("Hello, world!\n\n");
  char ch = getch();

  printw("The pressed key is ");
  attron(A_BOLD);
  printw("%c\n", ch);
  attroff(A_BOLD);

  refresh();
  getch();
  endwin();

  return 0;
}